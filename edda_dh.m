function Kdh = edda_dh(q1, q2, l1, l2)

    Kdh = [cos(q1+q2), -sin(q1+q2), 0, (l1*cos(q1))+(l2*cos(q1+q2));
        sin(q1+q2), cos(q1+q2), 0, (l1*sin(q1)) + (l2*sin(q1+q2));
        0, 0, 1, 0;
        0, 0, 0, 1
    ];

end