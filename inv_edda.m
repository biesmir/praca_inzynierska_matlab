
function q = inv_edda(K, l)

cq1 = (K(1, 4) - K(1, 1)*l(2))/l(1);
sq1 = (K(2, 4) - K(2, 1)*l(2))/l(1);

if sq1 > 0
    q1 = acos(cq1);
else
    q1 = -acos(cq1);
end

if K(2, 1) > 0
    q12 = acos(K(1, 1))
else
    q12 = -acos(K(1, 1))
end


q2 = q12 - q1;


q = [q1;q2];
end