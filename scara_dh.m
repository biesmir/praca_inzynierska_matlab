function K = scara_dh(q1, q2, q3, q4, a1, a2, d1)
    K = [cos(q1+q2+q4), -sin(q1+q2+q4), 0, a1*cos(q1) + a2*cos(q1+q2);
            sin(q1+q2+q4), cos(q1+q2+q4), 0, a1*sin(q1) + a2*sin(q1+q2);
            0,0,1, d1+q3;
            0,0,0,1
    ];
end