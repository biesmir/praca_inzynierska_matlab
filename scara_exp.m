
function K = scara_exp(q1, q2, q3, q4, a1, a2, d1)

    e3 = [0,-1,0;
        1,0,0;
        0,0,0];

    e2 = [0,0,1;
        0,0,0
        -1,0,0
        ];

    K0 = [eye(3),[(a1+a2);0;d1]
        zeros(1,3),1
    ];

    ksi1 = [e3,zeros(3,1);
        zeros(1,4)];

    ksi3 = [zeros(3), [0;0;1];
        zeros(1,4)];

    dz1 = ksi1;
    dz2 = [e3, [0;-a1;0];
        zeros(1,4)
        ];
    dz3 = ksi3;
    dz4 = [e3, [0;-(a1+a2);0];
        zeros(1,4)
        ];

    K = expm(dz1*q1)*expm(dz2*q2)*expm(dz3*q3)*expm(dz4*q4)*K0;
end